@extends('layouts.app')

@section('content')

{{ Breadcrumbs::render('categories/edit') }}

<form action="{{ url('/admin/categories/update') }}" method="POST">
    {{ csrf_field() }}

    <div class="row">
        <div class="col-lg-12">
            <div class="card bg-light mb-2">
                <div class="card-body">
                    <h4 class="card-title">1. Descrição</h4>
                    <div class="form-group">
                        <textarea name="description" id="desc" cols="30" rows="10" class="form-control">{!! $category->description !!}</textarea>
                        <input type="hidden" name="id" value="{{ $category->id}}">
                    </div>
                </div>
            </div>
            <div class="card bg-light mb-5">
                <div class="card-body">
                    <h4 class="card-title">2. Imagem de destaque</h4>
                    <div class="form-group">
                        Selecione uma imagem de destaque p/ a categoria. Tamanho máximo do arquivo: 2mb
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile04" aria-describedby="inputGroupFileAddon04">
                                <label class="custom-file-label" for="inputGroupFile04">Selecione uma imagem</label>
                            </div>
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-secondary" type="button" id="uploadFile"><i class="fa fa-arrow-up"></i> Upload</button>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <input type="text" name="alt_text" id="alttext" class="form-control" placeholder="Ex: Imagem de uma resistência tubular..." value="{{ $category->alt_text}}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-lg btn-block btn-primary">Atualizar</button>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')

    <script src="{{ asset('/vendor/summernote/dist/summernote-bs4.js') }}"></script>
    <script>
        $("textarea").summernote({
            height: 300
        });
    </script>
@endpush

@push('styles')

    <link rel="stylesheet" href="{{ asset('/vendor/summernote/dist/summernote-bs4.css') }}">

@endpush