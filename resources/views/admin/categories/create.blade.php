@extends('layouts.app')

@section('content')

{{ Breadcrumbs::render('create') }}

<form action="{{ url('/admin/categories/store') }}" method="POST">
    {{ csrf_field() }}
    
    <div class="row">
        <div class="col-lg-12">
            <div class="card bg-light mb-2">
                <div class="card-body">
                    <h4 class="card-title">1. Nome da categoria</h4>
                    <div class="form-group">
                        <label for="name">Defina um nome para a categoria</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Ex: Resistência Tubular..." required>
                    </div>
                </div>
            </div>
            <div class="card bg-light mb-2">
                <div class="card-body">
                    <h4 class="card-title">2. Descrição da categoria</h4>
                    <div class="form-group">
                        <label for="desc">Defina a descrição da categoria</label>
                        <textarea name="description" id="desc" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="card bg-light mb-5">
                <div class="card-body">
                    <h4 class="card-title">3. Imagem de destaque</h4>
                    <div class="form-group">
                        <label for="image">Escolha uma imagem de destaque</label>
                        <input type="file" 
                                    class="filepond js-category-image"
                                    name="images" 
                                    id="files"
                                    multiple 
                                    data-max-file-size="2MB"
                                    data-max-files="1">
                                    <input type="hidden" name="image_id" id="imageId">
                    </div>
                    <div class="form-group">
                        <label for="alttext">Texto alternativo</label>
                        <input type="text" id="alttext" name="alt_text" class="form-control" placeholder="Ex: Imagem de um pássaro cantando">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-lg btn-block btn-primary">Cadastrar</button>
            </div>
        </div>
    </div>
</form>

@endsection

@push('scripts')

    <script src="{{ asset('/vendor/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/dist/filepond.min.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/plugins/filepond.jquery.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/plugins/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/plugins/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.min.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/plugins/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.min.js') }}"></script>
    <script>
        // summernote
        $("textarea").summernote({
            height: 300
        });

        // filepond
        $.fn.filepond.registerPlugin(FilePondPluginFileValidateSize);
        $.fn.filepond.registerPlugin(FilePondPluginFileValidateType);
        $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
        $(".js-category-image").filepond({
            allowMultiple: false,
            allowRevert: true,
            maxFiles: 1,
            labelMaxFileSizeExceeded: "Este arquivo é muito grande!",
            labelMaxFileSize: "O tamanho máximo para arquivos é {filesize}",
            acceptedFileTypes: ['image/png', 'image/jpeg'],
            labelFileTypeNotAllowed: "Somente arquivos do tipo imagem são permitidos",
            fileValidateTypeLabelExpectedTypes: "Esperado {allButLastType} ou {lastType}",
            server: {
                url: "{{ url('/') }}",
                process: {
                    url: "/admin/images/store",
                    timeout: 7000,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function (response) {
                        var obj = JSON.parse(response);
                        $("#imageId").val(obj.data.id).trigger("change");                                              
                    }
                },
                revert: {
                    url: "/admin/images/delete/",
                    timeout: 7000,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function (response) {
                        console.log(response);
                        alert("arquivo deletado");
                    }
                }
            }
        });

        $(document).on("change", "#imageId", function () {
            console.log($(this).val())
        });
    </script>
@endpush

@push('styles')

    <link rel="stylesheet" href="{{ asset('/vendor/summernote/dist/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/filepond/dist/filepond.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/filepond/plugins/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css') }}">

@endpush