@extends('layouts.app')

@section('content')

    <!-- Breadcrumbs-->
    {{ Breadcrumbs::render('categories') }}

    <div class="row">
        <div class="col-lg-12">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card mb-5">
                <div class="card-body">
                    <a href="{{ url('/admin/categories/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Adicionar novo</a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered" id="categoriesList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Criado em</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- rows -->
                    @foreach ($categories as $category)
                        <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->created_at->format('d/m/Y H:m:s') }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{!! url("/admin/categories/edit/{$category->id}") !!}" data-id="{{ $category->id }}" class="btn btn-secondary"><i class="fa fa-edit"></i> Editar</a>
                                <button id="deleteCategory" data-id="{{ $category->id }}" type="button" class="btn btn-danger js-delete-item">
                                    <i class="fa fa-trash-alt"></i> Excluir
                                </button>      
                            </div>
                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('scripts')

    <script src="{{ asset('/vendor/bootbox/bootbox.min.js') }}"></script>
    <script>
        // Inicia o datatables
        $("#categoriesList").DataTable();

        // envia o header à todas requisições ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Função deletar categoria
        $(".js-delete-item").on("click", function () {
            var itemId = $(this).data("id");
            var row = $(this).parents().eq(2);
            
            bootbox.dialog({
                title: "Confirmar exclusão de categoria",
                message: "Ao deletar esta categoria, você estará deletando também <b>todos</b> os produtos ligados à ela, esta ação não poderá ser desfeita, deseja continuar?",
                size: "large",
                buttons: {
                    cancel: {
                        label: "Cancelar",
                        className: "btn-secondary"
                    },
                    ok: {
                        label: "<i class ='fa fa-trash-alt'></i> Excluir",
                        className: "btn-danger",
                        callback: function () {
                            $.ajax({
                                url: "/admin/categories/delete",
                                type: "DELETE",
                                dataType: "json",
                                data: { id: itemId },
                                success: function (data) {
                                    if (data.error) {
                                        bootbox.alert(data.error);
                                    }

                                    bootbox.alert(data.success);
                                    row.remove();
                                },
                                error: function (response) {
                                    bootbox.alert(`Erro ${response.status}: ${response.statusText}`);
                                    console.log(response.statusText);
                                }
                            });
                        }
                    }
                }
            });
        });
    </script>

@endpush