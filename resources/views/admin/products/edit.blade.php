@extends('layouts.app')

@section('content')

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">
            <a href="{{ url('/admin/products') }}">Produtos</a>
        </li>
        <li class="breadcrumb-item active">Editar</li>
    </ol>

    <form action="{{  url('/admin/products/update') }}" method="POST">
        {{ csrf_field() }}

        <div class="row">
            <div class="col-lg-12">
                <div class="card bg-light mb-2">
                    <div class="card-header">
                        Editando o produto: {{ $product->name }}
                    </div>
                    <div class="card-body">
                        <label for="name">Nome do produto</label>
                        <input type="text" id="name" name="name" class="input-lg form-control" value="{{ $product->name }}" disabled>
                        <input type="hidden" name="product_id" value="{{ $product->id}}">
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <label for="quant">Quantidade</label>
                        <input type="number" name="quantity" id="quant" min="1" value="{{ $product->quantity }}" class="input-lg form-control">
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <label for="category">Categoria do produto</label>
                        <select name="category" id="category" class="form-control js-select2">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ $productCategory->id == $category->id? 'selected' : '' }}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <label for="description">Descrição do produto</label>
                        <textarea name="description" id="description" cols="30" rows="10" class="form-control">{!! $product->description !!}</textarea>
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <label for="specifications">Especificações técnicas</label>
                        <textarea name="specifications" id="specifications" cols="30" rows="10" class="form-control">{!! $product->specifications !!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-lg btn-block">Atualizar</button>
                </div>
            </div>
        </div>
    </form>

@endsection

@push('scripts')

    <script src="{{ asset('/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/vendor/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('/vendor/bootbox/bootbox.min.js') }}"></script>
    <script>
        $("#category").select2({
            theme: "bootstrap"
        });

        $("textarea").summernote({
            height: 300
        });
    </script>
@endpush

@push('styles')

    <link rel="stylesheet" href="{{ asset('/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/select2/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/summernote/dist/summernote-bs4.css') }}">

@endpush