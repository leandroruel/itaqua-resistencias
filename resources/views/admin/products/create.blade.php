@extends('layouts.app')

@section('content')

    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">
            <a href="{{ url('/admin/products') }}">Produtos</a>
        </li>
        <li class="breadcrumb-item active">Adicionar novo</li>
    </ol>

    <div class="row">
        <div class="col-lg-12">
           
            <form action="{{ url('/admin/products/store') }}" method="POST">
                {{ csrf_field() }}

                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="card-title text-muted">1. Nome & Quantidade</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-xs-12">
                                <label for="name">Nome do produto:</label>
                                <input type="text" name="name" id="name" class="form-control" required>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <label for="quantity">Quantidade em estoque</label>
                                <input type="number" min="1" name="quantity" id="quantity" value="1" class="form-control" required>
                            </div>
                        </div>                        
                    </div>
                </div>
                
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="card-title text-muted">2. Categoria</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">                                
                                <label for="category">Categoria do produto</label>
                                <select name="category" id="category" class="form-control" required>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="card-title text-muted">3. Descrição do produto</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="description">Descrição do produto</label>
                                    <textarea name="description" id="description" cols="30" rows="10" class="form-control description"></textarea>   
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="card-title text-muted">4. Especificações técnicas</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">                                
                                <label for="specifications">Especificações técnicas</label>
                                <textarea name="specifications" id="specifications" cols="30" rows="10" class="form-control specifications"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h3 class="card-title text-muted">5. Carregue as imagens</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 js-image-upload">
                                <label for="files">Adicione as imagens do produto</label>
                                <p class="text-info"><small>clique na área abaixo ou arraste arquivos para adicionar</small></p>    
                                <input type="file" 
                                    class="filepond product-images"
                                    name="images" 
                                    id="files"
                                    multiple 
                                    data-max-file-size="3MB"
                                    data-max-files="3">
                                <select name="image_id[]" id="imagesId" style="display:none" multiple>
                                    <!-- add options here -->
                                </select>
                            </div>
                        </div>
                    </div>    
                </div>
                
                <div class="form-group">
                    <button class="btn btn-primary btn-lg btn-block" type="submit" id="sendForm">Cadastrar</button>
                </div>
            </form>
               
        </div>
    </div>

@endsection

@push('scripts')

    <script src="{{ asset('/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/vendor/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/dist/filepond.min.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/plugins/filepond.jquery.js') }}"></script>
    <script src="{{ asset('/vendor/filepond/plugins/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.js') }}"></script>
    <script>
        // passa o csrf token para chamadas ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Select2 plugin
        $("#category").select2({
            theme: "bootstrap",
            allowClear: true,
            placeholder: "Selecione uma opção",
            tags: true,
            tokenSeparators: [',', ' '],
        });

        // Summernote plugin
        $("textarea").summernote({
            tabsize: 2,
            height: 300
        });

        // Filepond plugin
        // First register any plugins
        $.fn.filepond.registerPlugin(FilePondPluginImagePreview);

        // Turn input element into a pond
        $(".product-images").filepond({
            allowMultiple: true,
            allowRevert: false,
            maxFiles: 6,
            server: {
                url: "{{ url('/') }}",
                process: {
                    url: "/admin/images/store",
                    timeout: 7000,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function (response) {
                        var obj = JSON.parse(response);
                        $("#imagesId").append(`<option value="${obj.data.id}" selected>${obj.data.filename}</option>`);                                              
                    }
                },
                revert: {
                    url: "/admin/images/delete",
                    timeout: 7000,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: function (response) {
                        console.log(response);
                    }
                }
            }
        });



        // Listen for addfile event
        $(".product-images").on("FilePond:addfile", function(e) {
            console.log(e);
        });
    

        // Send form
        $("form").on("submit", function () {
            var description = $("#description").summernote("isEmpty");
            var specifications = $("#specifications").summernote("isEmpty");

            if (description) {
                alert("O campo descrição é obrigatório");
                console.error("O campo descrição não pode estar vazio.");
                return false;
            }

            if (specifications) {
                alert("O campo especificações técnicas é obrigatório");
                console.error("O campo especificações técnicas é obrigatório");
                return false;
            }

            return true;
        });
    </script>

@endpush

@push('styles')

    <link rel="stylesheet" href="{{ asset('/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/select2/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/summernote/dist/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/filepond/dist/filepond.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/filepond/plugins/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css') }}">
    <style>
        .select2-container--bootstrap .select2-selection--multiple {
            min-height: 40px;
        }
        .select2-container--bootstrap .select2-selection--multiple .select2-selection__rendered {
            line-height: 1.9;
        }
        .select2 {
            width: 100% !important;
        }
    </style>
@endpush