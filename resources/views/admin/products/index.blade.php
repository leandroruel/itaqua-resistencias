@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs-->
    {{ Breadcrumbs::render('products') }}

    @if($categories->isEmpty())

     <div class="alert alert-info">
         <h4 class="alert-heading">Nenhuma categoria cadastrada</h4>
         <p>No momento você não possui categorias, para começar a cadastrar produtos, primeiro você precisa criar categorias</p>
        <p><a href="{{ url('/admin/categories/create') }}" class="btn btn-primary">Clique aqui para criar sua primeira categoria</a></p>
     </div>

    @else

    <div class="row">
        <div class="col-lg-12">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    </div>

    <!-- section title -->
    <div class="row mb-5">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('/admin/products/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Adicionar novo</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
             <!-- products list -->
            <div class="card">
                <div class="card-header">Produtos cadastrados</div>
                <div class="card-body">
                    <table class="table table-bordered" id="productsList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Quantidade</th>
                                <th>Criado em</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td><a href="#" data-id="{{ $item->id }}" title="Ver detalhes" class="item-details">{{ $item->name }}</a></td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>{{ $item->created_at->format('d/m/Y H:m:s') }}</td>
                                    <td>
                                        <div class="btn-group">
                                        <a href="{!! url("/admin/products/edit/{$item->id}") !!}" class="btn btn-primary btn-action btn-edit" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="top" title="Editar produto">
                                                <i class="fa fa-edit"></i> Editar
                                            </a>
                                            <button type="button" class="btn btn-danger btn-action btn-delete" data-id="{{ $item->id }}" data-toggle="tooltip" data-placement="top" title="Excluir produto">
                                                <i class="fa fa-trash-alt"></i> Excluir
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>    
        </div>
    </div>
    @endif
 
@endsection

@push('scripts')
    <script src="{{ asset('/vendor/bootbox/bootbox.min.js') }}"></script>
    <script>
        // Inicializa o datatables
        $("#productsList").DataTable();

        // Tooltips
        $(".btn-action").tooltip();

        // Ver detalhes do produto
        $(".item-details").on("click", function (e) {
            e.preventDefault();

            var itemId = $(this).data("id");

            $.ajax({
                url: "/admin/products/details",
                type: "GET",
                dataType: "json",
                data: { id: itemId },
                success: function (data) {
                    console.log(data)
                    if (data.error) {
                        bootbox.alert(data.error);
                    }

                    bootbox.dialog({
                        title: "Detalhes do item",
                        message: `<h4>${data.name}</h4>
                        <p><strong>Quantidade:</strong> ${data.quantity}</p>
                        <div class='accordion' id='accordion${data.id}'>
                            <div class='card'>
                                <div class='card-header' id='heading${data.id}'>
                                    <h5 class='mb-0'>
                                        <button type='button' class='btn btn-link' data-toggle='collapse' data-target='#collapse${data.id}' aria-expanded='false' aria-controls='collapse${data.id}'>
                                            Ver Descrição
                                        </button>
                                    </h5>
                                </div>
                                <div id='collapse${data.id}' class='collapse' data-parent='#accordion${data.id}'>
                                    <div class='card-body'>
                                        ${data.description}
                                    </div>
                                </div>
                            </div>
                            <div class='card'>
                                <div class='card-header' id='heading${(data.id + 1)}'>
                                    <h5 class='mb-0'>
                                        <button type='button' class='btn btn-link' data-toggle='collapse' data-target='#collapse${(data.id + 1)}' aria-expanded='false' aria-controls='collapse${(data.id + 1)}'>
                                            Ver Especificações
                                        </button>
                                    </h5>
                                </div>
                                <div id='collapse${data.id+1}' class='collapse' data-parent='#accordion${data.id}'>
                                    <div class='card-body'>
                                        ${data.specifications}
                                    </div>
                                </div>
                            </div>
                        </div>`
                    });
                },
                error: function (xhr) {
                    bootbox.alert(`Erro ${xhr.status}: ${xhr.statusText}`);
                }
            });
        });

        // deletar produto
        $(".btn-delete").on("click", function () {
            var row = $(this).parents().eq(2);
            var itemId = $(this).data("id");

            bootbox.dialog({
                title: 'Tem certeza que deseja excluir este produto?',
                message: 'Ao excluir este produto você irá removê-lo definitivamente e todo o estoque. O produto também deixará de ser exibido em sua página de produtos, esta ação não pode ser revertida.',
                size: 'large',
                buttons: {
                    ok: {
                        label: "<i class='fa fa-trash-alt'></i> Excluir",
                        className: "btn-danger",
                        callback: function () {
                            $.ajax({
                                url: "/admin/products/delete",
                                type: "GET",
                                dataType: "json",
                                data: {id: itemId},
                                success: function (data) {
                                    if (data.error) {
                                        bootbox.alert(data.error);
                                    } else {
                                        bootbox.alert(data.success);
                                        row.remove();
                                    }
                                },
                                error: function (response) {
                                    bootbox.alert(`Erro ${response.status}: ${response.statusText}`);
                                }
                            });
                        }
                    },
                    cancel: {
                        label: "Cancelar",
                        className: "btn-default"
                    }
                }
            });            
        });
    </script>

@endpush