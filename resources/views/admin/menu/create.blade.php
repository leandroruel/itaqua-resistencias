@extends('layouts.app')

@section('content')

    <form action="{{ url('/admin/menu/store') }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-12">
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <h4 class="card-title">1. Nome do item</h4>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-lg" placeholder="Ex: Produtos" required>
                        </div>
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <h4 class="card-title">2. Url do item</h4>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">{{ url('/')}}</span>
                                </div>
                                <input type="url" id="link" name="url" class="form-control" list="defaultUrls" placeholder="Ex: /noticias ou /noticias/politica" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <h4 class="card-title">3. Visibilidade</h4>
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="active" id="active" class="form-check-input" checked>
                                <label for="active" class="form-check-label">Ativado</label>
                            </div>
                            <div class="form-check">
                                <input type="radio" name="active" id="notactive" class="form-check-input">
                                <label for="notactive" class="form-check-label">Desativado</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <h4 class="card-title">4. Item filho ou pai?</h4>
                        <div class="form-group">
                            <label for="parentLink">Definir se o item é um sub-item, se não, deixe esta opção em branco</label>
                            <select name="parent" id="parentLink" class="form-control">
                                <option value="0">Selecionar</option>
                            </select>
                        </div>  
                    </div>
                </div>
                <div class="card bg-light mb-2">
                    <div class="card-body">
                        <h4 class="card-title">5. Ordem</h4>
                        
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@push('scripts')
    
@endpush

@push('styles')
    <style>
        .url-list {
            position: relative;
            z-index: 1;
            height: 40px;
        }

        .url-list .suggested-urls {
            width: 100%;
            display:none;
            background: #fff;
            border: 1px solid #eee;
            box-shadow: 2px 1px 3px rgba(0,0,0,.3);
            position: absolute;
            top: 100%;
            left: 0;
        }
        .url-list .suggested-urls li {
            display: flex;
            flex-direction: row;
            justify-content: space-around;
            align-items: center;
            padding: 15px 10px;
            cursor: default;
            color: #333;
        }
        .url-list .suggested-urls li:hover {
            background-color: #eee;
        }
    </style>
@endpush