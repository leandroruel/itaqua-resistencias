@extends('layouts.app')

@section('content')

<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Menu</li>
</ol>

<div class="row">
    <div class="col-lg-12">
        <div class="card mb-5">
            <div class="card-body">
                <a href="{{ url('/admin/menu/create') }}" class="btn btn-primary">Adicionar item <i class="fa fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card bg-light mb-2">
            <div class="card-header">Items do menu principal do site</div>
            <div class="card-body">
                <table class="table table-bordered" id="listItems">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Link</th>
                            <th>Ordem</th>
                            <th>Ativo</th>
                            <th>Criado em</th>
                            <th>Atualizado em</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection