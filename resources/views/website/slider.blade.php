<div class="glide main-slider" id="mainSlider">
    <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides home-slides">
            <li class="glide__slide">
                <img src="{{ asset('/images/carousel/banner1.jpg') }}" alt="">
            </li>
            <li class="glide__slide">
                <img src="{{ asset('/images/carousel/banner2.jpg') }}" alt="">
            </li>
        </ul>
    </div>
</div>

@prepend('scripts')
    <script src="https://cdn.jsdelivr.net/npm/@glidejs/glide"></script>
    <script>
        new Glide('#mainSlider', {
            gap: 0,
            autoplay: 4000,
            hoverpause: true
        }).mount()
    </script>
@endprepend
