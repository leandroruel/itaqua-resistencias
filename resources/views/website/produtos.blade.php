@extends('website.layouts.layout')

@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li>Produtos</li>
</ol>

<section class="products">
    <div class="products__list">
        @foreach ($categories as $item)
        
            <div class="card card--bordered" style="max-width:380px">
                
                <img src="{{ url($item->images->first()->path . '/' .$item->images->first()->name .'.'. $item->images->first()->extension) }}" alt="" class="card__image">
                
                <div class="card__body">
                    <div class="card__title">
                        <a href="{{ url('produtos/' .$item->slug) }}">{{ $item->name }}</a>
                    </div>
                </div>
                <div class="card__buttons">
                    <a href="{{ url('produtos/' .$item->slug) }}" class="button button--primary button--block">ver detalhes</a>
                </div>
            </div>
        @endforeach
    </div>
</section>

@endsection
