<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Itaqua Resistências</title>
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">

    @stack('styles')
</head>
<body>
    <header class="header">
        <!-- info area -->
        <div class="header__info">
            <div class="header__container">
                <ul class="header__contact">
                    <li><i class="fa fa-phone"></i> +55 11 4755 1022</li>
                    <li><i class="fa fa-clock"></i> Seg a Sex das 07:00 às 17:00</li>
                    <li><i class="fa fa-location-arrow"></i> Av Cardeal, 351 - Pq São Pedro - Itaquaquecetuba</li>
                </ul>
                <ul class="header__social-media">
                    <li><a href="mailto:vendas&#64;itaquaresistencias.com.br" target="_blank"><i class="fa fa-envelope"></i></a></li>
                    <li><a href="https://www.facebook.com/itaquaresistencias" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://www.twitter.com/itaquaresistencias" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://linkedin.com/" target="_blank"><i class="fab fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>

        <!-- logo area -->
        <div class="header__logo">
            <div class="header__container">
                <a href="{{ url('/') }}" class="logo__link">
                    <img src="{{ asset('/images/logo_black.jpg') }}" class="header__logo-img" alt="Logo Itaquá Resistências">
                </a>
                
                <div class="header__buttons">
                    <!-- search button -->
                    <button type="button" class="header__sbutton" id="searchButton">
                        <i class="fas fa-search"></i>
                    </button>
                    <!-- mobile button -->
                    <button aria-expanded="false" type="button" class="header__mbutton">
                        <i class="fas fa-bars"></i>
                    </button>
                </div>        
            </div>            
        </div>        
    </header>

    <nav role="navigation" id="mainNav" aria-label="Main menu" class="nav">
        <button type="button" class="nav__close"><i class="fa fa-times"></i></button>
        <ul class="menu" role="menubar" aria-hidden="false">
            <li class="menu__item" role="menuitem" aria-haspopup="false">
                <a href="{{ url('/') }}" tab-index="-1" class="menu__link">Home</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="true">
                <a href="{{ url('/produtos') }}" tab-index="-1" class="menu__link">Produtos</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="true">
                <a href="{{ url('/segmentos') }}" tab-index="-1" class="menu__link">Segmentos</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="true">
                <a href="{{ url('/maquinas') }}" tab-index="-1" class="menu__link">Máquinas</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="false">
                <a href="{{ url('/blog') }}" tab-index="-1" class="menu__link">Blog</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="false">
                <a href="{{ url('/empresa') }}" tab-index="-1" class="menu__link">Empresa</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="false">
                <a href="{{ url('/eventos') }}" tab-index="-1" class="menu__link">Eventos</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="true">
                <a href="{{ url('/informacoes') }}" tab-index="-1" class="menu__link">Informações</a>
            </li>
            <li class="menu__item" role="menuitem" aria-haspopup="false">
                <a href="{{ url('/contato') }}" tab-index="-1" class="menu__link">Contato</a>
            </li>
        </ul>
    </nav>

    <main class="content">
        @if(Request::is('/'))
            @include('website.banner')
        @endif
        
        @yield('content')
    </main>

    <footer class="footer">
        <div class="footer__container">
            <div class="footer__column logo">
                <img src="{{ asset('/images/logo.png') }}" alt="Logo Itaqua Resistências" class="footer__logo">
                <p>Fundada em 2007 a Itaquá Resistências é uma empresa altamente qualificada no desenvolvimento de resistências elétricas para toda e qualquer aplicação.</p>
            </div>
            <div class="footer__column">
                <h4 class="footer__title">links úteis</h4>
                <ul class="footer-nav">
                    <li class="footer-nav__item">
                        <a href="{{ url('/produtos')}}" class="footer-nav__link">nossos produtos</a>
                    </li>
                    <li class="footer-nav__item">
                        <a href="{{ url('/contato') }}" class="footer-nav__link">contato</a>
                    </li>
                    <li class="footer-nav__item">
                        <a href="{{ url('/empresa') }}" class="footer-nav__link">empresa</a>
                    </li>
                    <li class="footer-nav__item">
                        <a href="{{ url('/eventos') }}" class="footer-nav__link">eventos</a>
                    </li>
                    <li class="footer-nav__item">
                        <a href="{{ url('/sitemap') }}" class="footer-nav__link">mapa do site</a>
                    </li>
                </ul>
            </div>
            <div class="footer__column">
                <h4 class="footer__title">contato</h4>
                <ul class="footer-nav">
                    <li class="footer-nav__item"><i class="fa fa-location-arrow"></i>  Av. Cardeal, 351 - Pq. São Pedro Itaquaquecetuba, SP</li>
                    <li class="footer-nav__item"><i class="fa fa-phone"></i> +55 11 4755 1022</li>
                    <li class="footer-nav__item"><i class="fab fa-whatsapp"></i> +55 11 95234 3180</li>
                    <li class="footer-nav__item"> <i class="fa fa-envelope"></i> vendas&#64;itaquaresistencias.com.br</li>
                </ul>
            </div>
        </div>
        <div class="footer__copyright">
            Copyright © {{ date('Y') }} Itaquá Resistências. 
        </div>
    </footer>

    @stack('scripts')
    <script src="{{ asset('/js/menu.js') }}"></script>
</body>
</html>
