@extends('website.layouts.layout')

@section('content')

    <section>
        <div class="contact animatedParent">
            <div class="contact__info animated fadeInUpShort">
                <div class="contact__icon">
                    <i class="fa fa-phone"></i>
                </div>
                <h2 class="contact__title">Telefone</h2>
                <p>Nossos atendentes estão sempre prontos para atênde-lo, ligue agora.</p>
                <p><a href="tel:1147551022">(11) 4755-1022</a></p>
            </div>
            <div class="contact__info animated fadeInUpShort">
                <div class="contact__icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <h2 class="contact__title">Email</h2>
                <p>Nossa central de vendas também atende via e-mail.</p>
                <p><a href="mailto:vendas@itaquaresistencias.com.br">Vendas@itaquaresistencias.com.br</a></p>
            </div>
            <div class="contact__info animated fadeInUpShort">
                <div class="contact__icon">
                    <i class="fa fa-location-arrow"></i>
                </div>
                <h2 class="contact__title">Localização</h2>
                <p>Avenida Cardeal, 351 - Parque São Pedro, Itaquaquecetuba - SP 08586-010</p>
                <p><a href="https://goo.gl/maps/VXaS4unarjo">Ver no Google Maps</a></p>
            </div>
        </div>
    </section>
    <section>
        <form class="form contact-form animatedParent" method="post" action="{{ url('/contato/store') }}">  
             {{ csrf_field() }}
            @if(session('success'))
                <div class="form__message">
                    {{ session('success') }}
                </div>
            @endif  

            <div class="form__container animated fadeInUpShort">
                <h1>Deixe-nos uma mensagem</h1>
                <div class="form__group">
                    <input type="text" name="nome" class="form__input form__input--large" placeholder="Nome" required>
                </div>
                <div class="form__group">
                    <input type="email" name="email" id="" class="form__input form__input--large" placeholder="E-mail" required>
                </div>
                <div class="form__group">
                    <input type="tel" name="telefone" id="" class="form__input form__input--large" placeholder="Telefone" required>
                </div>
                <div class="form__group">
                    <label for="subject" class="form__label">Como nos conheceu?</label>
                    <select name="assunto" id="subject" class="form__input">
                        <option value="google">Busca no google</option>
                        <option value="outros buscadores">Outros Buscadores</option>
                        <option value="link patrocinado">Links patrocinados</option>
                        <option value="outro anúncio">Outros anúncios</option>
                        <option value="facebook">Facebook</option>
                        <option value="indicado">Indicação</option>
                        <option value="outros">Outros</option>
                    </select>        
                </div>
                <div class="form__group">
                    <textarea name="descricao" id="" cols="30" rows="10" class="form__input"></textarea>
                </div>
                <div class="form__group">
                    <button class="button button--large button--block button--primary">Enviar</button>
                </div>                
            </div>
        </form>   
    </section>

    <section>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.560879916572!2d-46.34336068502451!3d-23.440224684743217!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce7d18e1350fa1%3A0xdd2ab0b219788580!2sAvenida+Cardeal%2C+351+-+Parque+Sao+Pedro%2C+Itaquaquecetuba+-+SP%2C+08586-010!5e0!3m2!1spt-BR!2sbr!4v1489674108899" width="100%" height="500" style="border:0" allowfullscreen=""></iframe>
    </section>

@endsection

@push('scripts')

    <script src="{{ url('/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('/vendor/css3-animate-it/js/css3-animate-it.js') }}"></script>
    
@endpush

@push('styles')

    <link rel="stylesheet" href="{{ url('/vendor/css3-animate-it/css/animations.css') }}">
    <style>
        .gmaps-full {
            height: 400px;
        }
    </style>

@endpush