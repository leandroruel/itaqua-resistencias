@extends('website.layouts.layout')

@section('content')

    <section>
        <div class="featured-services">
            <div class="featured-services__text">
                <h1 class="title title--thin no-margin p-bottom-10">Para sua indústria</h1>
                <img src="{{ url('/images/layout/service.jpg') }}" alt="">
                <p>Conheça a gama de produtos da Itaquá Resistências para poder realizar com muito mais eficiência,
                    uniformidade e rapidez quaisquer processos de transferência de calor. São modelos de altíssima qualidade,
                    ideais para o uso em qualquer indústria e até nas situações mais complexas. Entre em contato e solicite um
                    orçamento!
                </p>
                <a href="{{ url('/contato') }}" class="button button--secondary">Peça um orçamento</a>
            </div>
            <div class="featured-services__group">
                <h1 class="title title--thin no-margin p-bottom-50">Porque nos escolher?</h1>
                <div class="card">
                    <div class="card__body">
                        <h4> <i class="fa fa-clock"></i> Entregas pontuais</h4>
                        <p>Seu pedido entregue no prazo</p>
                    </div>
                </div>
                <div class="card">
                    <div class="card__body">
                        <h4>Pioneiros em Resistências</h4>
                        <p>há anos suprindo as necessidades da indústria</p>
                    </div>
                </div>
                <div class="card">
                    <div class="card__body">
                        <h4>Alto padrão de qualidade</h4>
                        <p>aprovado por nossos clientes</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="machines">
            <h1 class="title title--thin title--divider-right">Máquinas</h1>
            <p>
                As resistências elétricas da Itaquá, fornecem maior segurança e durabilidade para os maquinários,
                 independente do produto ou necessidade, do cliente. Preparadas para aguentar grandes temperaturas e de forma homogêneas,
                  elas são ideias para se atingir e exigir o máximo da sua máquina.
            </p>
            <div class="machines__cards">
                <div class="card">
                    <img src="{{ asset('/images/resistencia-extrusora.jpg') }}" class="card__image" alt="">
                    <div class="card__title">resistência para extrusora</div>
                    <div class="card__body">
                        A extrusão de plástico e qualquer outro material se torna mais uniforme, mais econômica,
                         mais padronizada e ainda muito mais rápida e ágil, com nossas resistências para extrusoras.
                    </div>
                    <div class="card__buttons">
                        <a href="#" class="button button--block">saiba mais →</a>
                    </div>
                </div><!-- /.card -->
                <div class="card">
                    <img src="{{ asset('/images/resistencia-empacotadeira.jpg') }}" class="card__image" alt="">
                    <div class="card__title">resistência para empacotadeira</div>
                    <div class="card__body">
                        As máquinas empacotadeiras, precisam ter uma boa performance, e por isso nossas resistências elétricas de ponta,
                             podem ser grandes aliados no empacotamento de seus produtos.
                    </div>
                    <div class="card__buttons">
                        <a href="#" class="button button--block">saiba mais →</a>
                    </div>
                </div><!-- /.card -->
                <div class="card">
                    <img src="{{ asset('/images/resistencia-hotstamping.jpg') }}" class="card__image" alt="">
                    <div class="card__title">resistência para hot stamping</div>
                    <div class="card__body">
                        As máquinas de hot stamping precisam de resistências elétricas de qualidade, para assegurar um aquecimento eficiente,
                             homogêneo e ágil de inúmeros materiais em um curto período de tempo.
                    </div>
                    <div class="card__buttons">
                        <a href="#" class="button button--block">saiba mais →</a>
                    </div>
                </div><!-- /.card -->
                <div class="card">
                    <img src="{{ asset('/images/resistencia-estufa.jpg') }}" class="card__image" alt="">
                    <div class="card__title">resistência para estufa</div>
                    <div class="card__body">
                        Para as estufas, um dos modelos mais adequados é a resistência tubular aletada.
                             Graças a suas aletas que pode distribuir ação térmica em qualquer tipo de estufaque conseguindo transmitir
                              calor até nos espaços mais pequenos e limitados.
                    </div>
                    <div class="card__buttons">
                        <a href="#" class="button button--block">saiba mais →</a>
                    </div>
                </div><!-- /.card -->
                <div class="card">
                    <img src="{{ asset('/images/resistencia-galvanizacao.jpg') }}" class="card__image" alt="">
                    <div class="card__title">resistência para galvanização</div>
                    <div class="card__body">
                        Para quem precisa aquecer um banho de galvanização, por exemplo,
                             seu portfólio contempla modelos especialmente adequados para uma fervura segura e homogênea dos metais envolvidos
                              no processo permitindo a formação de ligas específicas.
                    </div>
                    <div class="card__buttons">
                        <a href="#" class="button button--block">saiba mais →</a>
                    </div>
                </div><!-- /.card -->
                <div class="card">
                    <img src="{{ asset('/images/resistencia-caldeira.jpg') }}" class="card__image" alt="">
                    <div class="card__title">resistência para caldeiras</div>
                    <div class="card__body">
                        Se você deseja maximizar a performance e o desempenho de sua caldeira, conte com as resistências da Itaquá,
                            feitas com elevada qualidade, altíssimos graus de eficiência e matérias-primas de ponta para garantir uma
                              transferência homogênea.
                    </div>
                    <div class="card__buttons">
                        <a href="#" class="button button--block">saiba mais →</a>
                    </div>
                </div><!-- /.card -->
            </div>
        </div>
    </section>

    <section>
        <div class="company-segments">
            <div class="company-segments__featured">
                <h3>Titulo chamativo</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam provident dolores accusamus animi unde nihil praesentium quos. At illum magni eveniet voluptas odio quod. Temporibus adipisci excepturi praesentium similique blanditiis?</p>
            </div>
            <div class="company-segments__carousel">
                
            </div>
        </div>
    </section>

    <!-- clientes -->
    <section>
        <div class="clients">
            <div class="carousel owl-carousel owl-theme">
                <div><img src="{{ asset('/images/clientes/logo_petrobras.jpg') }}" alt="logo petrobrás"></div>
                <div><img src="{{ asset('/images/clientes/logo-bobs.jpg') }}" alt="logo bobs"></div>
                <div><img src="{{ asset('/images/clientes/logo-ajinomoto.jpg') }}" alt="logo ajinomoto"></div>
                <div><img src="{{ asset('/images/clientes/logo-brf.jpg') }}" alt="logo brf"></div>
                <div><img src="{{ asset('/images/clientes/logo-dori.jpg') }}" alt="logo dori"></div>
                <div><img src="{{ asset('/images/clientes/logo-garoto.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-bufalo.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-dixietoga.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-enova.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-erlan.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-eucatex.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-ferracini.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-fibria.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-garoto.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-gestamp.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-igui.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-incesa.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-induscabos.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-johnson.jpg') }}" alt=""></div>               
                <div><img src="{{ asset('/images/clientes/logo-mahle.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-marfinite.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-marilan.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-mauser.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-mondelez.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-moura.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-multilit.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-nambei.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-nestle.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-nsk.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-starmach.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-thevear.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-vale.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-valeo.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-vicunha.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-weg.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-wickbold.jpg') }}" alt=""></div>
                <div><img src="{{ asset('/images/clientes/logo-ype.jpg') }}" alt=""></div>
            </div>
        </div>
    </section>

@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('/vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('/vendor/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
<script>
     $('.carousel').owlCarousel({
         autoplay: true,
         loop: true,
         items: 7,
         dots: false,
         responsiveClass: true,
         responsive: {
             320: {
                 items: 2
             },
             600: {
                 items: 3
             },
             800: {
                 items: 4
             },
             1000: {
                 items: 4
             },
             1200: {
                 items: 6
             }
         }
     });
</script>

@endpush