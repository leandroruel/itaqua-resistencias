@extends('website.layouts.layout')

@section('content')
<h1>Máquinas</h1>
<p>A Itaquá Resistências fornece 17 modelos de resistências elétricas, ideais para o seu maquinário.
     Os produtos atendidos vão desde resistência para fornos industriais, galvanização, máquina extrusora, máquinas coladeiras de bordas,
      máquinas empacotadeiras, máquinas hots stamping, máquinas injetoras, marmiteiros, caldeiras, máquinas datadoras, máquinas estufas,
       sauna e sopradoras.
</p>

<p>As resistências elétricas da Itaquá, fornecem maior segurança e durabilidade para os maquinários, independente do produto ou necessidade,
     do cliente. Preparadas para aguentar grandes temperaturas e de forma homogêneas, elas são ideias para se atingir e exigir o máximo da sua máquina.
</p>

<p>Então, não deixe de utilizar resistências de alta durabilidade, garantindo a otimização e a segurança que o seu produto merece,
     sem causar dores de cabeça com eventuais falhas ou problemas técnicos de superaquecimento, que venham a acontecer e paralisar a produção ou funcionamento
      da sua linha de montagem ou do produto.
</p>
@endsection
