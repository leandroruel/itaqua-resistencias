@extends('website.layouts.layout')

@section('content')

    <div class="page-header page-about">
        <h1 class="page-header__title">Empresa</h1>
    </div>

    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li>Empresa</li>
    </ol>

    <section>
        <div class="about">
            <div class="about__image">
            
            </div>
            <div class="about__text">
                <p>Fundada em 2007 a Itaquá Resistências é uma empresa altamente qualificada no desenvolvimento de resistências elétricas para toda e qualquer
                    aplicação. Dispomos de profissionais experientes e preparados para produzirmos peças com alto nível de qualidade,
                    buscando sempre o aprimoramento de nossos produtos e a satisfação de nossos clientes, contamos com uma equipe de técnicos capacitados
                    em aquecimento que podem solucionar a sua necessidade.
                </p>
                <p>
                    Nossa experiência em campo em uma diversidade de indústrias permite-nos oferecer várias soluções e inovações tecnológicas em eletro termia.
                </p>
                <p>
                    Avaliação detalhada dos materiais recebidos, inspeções contínuas de processos internos e procedimentos de teste final
                    de cada item produzido, certifica os produtos livres de defeitos e problemas. Buscar e analisar novos materiais e métodos de fabricação
                    para aumentar a qualidade e eficiência. Testes de vida útil de produtos que fornecem avaliações de desempenho e melhoria de projeto.
                </p>
                <p>Agradecemos sua visita ao nosso site.</p>
            </div>
        </div>
    </section>
@endsection
    