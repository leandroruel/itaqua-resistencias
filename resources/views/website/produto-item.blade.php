@extends('website.layouts.layout')

@section('content')

<h1>{{ $name }}</h1>
{!! $description !!}
<h3>Especificações técnicas</h3>
{!! $specifications !!}
@endsection
