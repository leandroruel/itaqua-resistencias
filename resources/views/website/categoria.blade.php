@extends('website.layouts.layout')

@section('content')

<ol class="breadcrumb">
    <li><a href="{{ url('/') }}">Home</a></li>
    <li><a href="{{ url('/produtos') }}">Produtos</a></li>
    <li>{{ $category->name }}</li>
</ol>

<section class="category">
    <aside class="category__main" itemscope="http://schema.org/Category">
        <div class="category__img">
            <img src="{{ $category->images->first()->path . $category->images->first()->name . '.' . $category->images->first()->extension}}" alt="" itemprop="image">
        </div>
        <div class="category__text">
            <h1 class="category__name" itemprop="name">{{ $category->name }}</h1>
            <div class="category__description" itemprop="description">{!! $category->description !!}</div>
        </div>
        <a href="{{ url('/contato') }}" class="button button--block button--primary">Obter Cotação</a>
    </aside>

    <div class="category__products">

        @foreach($products as $product)
        
            <div class="card card--bordered" style="max-width: 300px;" itemscope="http://schema.org/Product">

                @foreach($product->images->where('is_default', 1) as $productImage)
                    
                    <img src="{{ $productImage->path . $productImage->name . '.' . $productImage->extension }}" alt="{{ $productImage->alt_text }}" itemprop="image">

                @endforeach

                <div class="card__body">
                    <div class="card__title" itemprop="name"><a href="" itemprop="name">{{ $product->name }}</a></div>
                    <div class="card__buttons">
                        <a href="{!! url("/produtos/{$category->slug}/{$product->slug}") !!}" class="button button--primary" itemprop="url">ver detalhes</a>
                    </div>
                </div>
            </div>

        @endforeach

    </div>
</section>

@endsection
