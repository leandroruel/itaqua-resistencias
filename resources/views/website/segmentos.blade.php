@extends('website.layouts.layout')

@section('content')
<h1>segmentos</h1>
<p>A Itaquá Resistências Elétricas fornece resistências elétricas ideais aos diversos segmentos da indústria.
     São insumos e complementos para cozinhas industriais, embalagens, galvanoplastia, indústria biomédicas,
      ar-condicionado, borracharia, resistências para indústria de metal de mecânica, plásticos e polímeros, refrigeração e degelo,
       lavanderias, panificação e indústria de óleo e gás.
</p>

<p>
    Estes segmentos de mercado utilizam resistências elétricas de diversas maneiras, desde modelos comuns no aquecimento de tambores,
     até mesmo com resistências de imersão, usadas em contato direto e submersão em líquidos.
</p>

<p>
    A aplicação das resistências elétricas nos setores da indústria, não são resumidas apenas no aquecimento dos materiais.
        A segurança e controle, como em cozinhas industriais, lavanderias e indústria de óleo e gás, indústria de refrigeração e até de ar-condicionado,
          é indispensável, fornecendo segurança e controle em algumas de suas etapas de produção.
</p>

<p>
    Dentro das máquinas, como as autoclaves, marmiteiros, sopradoras e caldeiras, as resistências aquecem com finalidades distintas: esterilizar, derreter, ferver e muitas outras aplicações em diversos materiais, como equipamentos cirúrgicos, plásticos e até borrachas.
</p>
@endsection
