<section class="hero">
<video loop autoplay src="{{ asset('/media/intro.mp4') }}" poster="{{ asset('/images/video_cover.jpg') }}" class="hero__video"></video>
    <div class="hero__backdrop">
        <div class="hero__container">
                <h1 class="hero__title">Resistência para todos</h1>
                <p class="hero__text">Seja qual for sua necessidade, conte com as resistências elétricas da Itaquá.
                     São produtos de altíssimo padrão confeccionados para garantirem excelência e precisão na transferência térmica em extrusoras,
                      injetoras, estufas, fornos industriais e quaisquer outras máquinas.
                </p>
                <a href="#" class="button button--primary button--large button--outline">saiba mais</a>
        </div>
    </div>
</section>
