@extends('website.layouts.layout')

@section('content')

    <h1>404 página não encontrada</h1>
    <p>a página que você procura não existe ou foi movida</p>

    @isset($exception)
        <p>{{ $exception->getMessage() }}</p>
    @endisset   
    

@endsection
