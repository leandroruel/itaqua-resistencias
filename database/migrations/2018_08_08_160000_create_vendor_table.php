<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->string('name', 30);
            $table->string('trade_name', 60);
            $table->integer('ie')->nullable();
            $table->string('address', 100);
            $table->string('city', 30);
            $table->string('state', 30);
            $table->string('country', 30);
            $table->string('cnpj', 14);
            $table->string('zip_code', 9);
            $table->string('phone', 12);
            $table->string('phone_2', 12);
            $table->string('email', 60);
            $table->string('owner_name', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor');
    }
}
