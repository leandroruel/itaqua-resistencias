<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImagePivotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_product_image')->insert([
            'product_id' => 1,
            'product_image_id' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => null
        ]);
    }
}
