<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_image')->insert([
            'name' => 'cover-resistencia-tubular-blindada',
            'path' => '/images',
            'alt_text' => '',
            'extension' => '.png',
            'is_default' => 1
        ]);
    }
}
