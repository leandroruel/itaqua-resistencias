<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductVendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_vendor')->insert([
            'name' => 'itaqua resistências',
            'trade_name' => 'ITAQUÁ IND. E COM. RESISTÊNCIAS ELÉTRICAS LTDA',
            'cnpj' => '08756067000162',
            'address' => 'av cardeal, 351 PQ São Pedro',
            'city' => 'Itaquaquecetuba',
            'state' => 'São paulo',
            'country' => 'Brasil',
            'zip_code' => '08580010',
            'phone' => '1147551022',
            'phone_2' => '',
            'owner_name' => 'clécio araujo',
            'email' => 'vendas@itaquaresistencias.com.br',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
