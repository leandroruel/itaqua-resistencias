<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product')->insert([
            'name' => 'Resistência Tubular Blindada',
            'description' => 'As resistências tubulares são peças de altíssimo poder térmico.',
            'specifications' => '<p>As resistências tubulares são peças de altíssimo poder térmico e calorífico que podem ser aplicadas em diversas situações, das estufas e do forno industrial aos equipamentos industriais mais complexos, como máquinas de embalagem, sopradoras e bicos injetores. A Itaquá oferece a versão blindada, que maximiza a performance das resistências convencionais com uma blindagem em aço ou outros materiais para melhorar os processos e reduzir as falhas.</p><p>O grande diferencial da blindagem é que, com esse revestimento extra, as resistências não perdem calor ao longo de seu uso, já que não há vazamentos ou riscos no acabamento. Assim, o aquecimento se torna mais homogêneo, preciso e uniforme, esquentando as superfícies com os melhores resultados sem, ao mesmo tempo, demandarem maior consumo de energia.</p><p>Consulte a Itaquá e solicite uma resistência tubular blindada com as especificidades, tamanhos e dimensões que sua superfície e seu maquinário precisar. Assim, você poderá aproveitar ao máximo as vantagens desse modelo, tendo um dia a dia mais tranquilo e melhor.</p>',
            'vendor_id' => 1,
            'slug' => 'resistencia-tubular-blindada',
            'is_active' => 1,
            'created_at' => date('Y-m-d H:m:s'),
            'updated_at' => null
        ]);
    }
}
