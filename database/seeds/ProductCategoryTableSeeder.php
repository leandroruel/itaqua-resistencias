<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_category')->insert([
            'name' => 'Resistências Tubulares',
            'slug' => 'resistencias-tubulares',
            'description' => 'As resistências tubulares da Itaquá, uma empresa 100% nacional que atua desde 2007 no mercado brasileiro, foram pensadas para garantirem o aquecimento das mais variadas aplicações industriais, em virtude de seu formato flexível',
            'image_path' => '/images/cover-resistencia-tubular-aletada-helicodal.jpg',
            'alt_text' => 'Imagem ilustrativa de Resistências Tubulares',
            'created_at' => date('Y-m-d H:m:s')
        ]);
    }
}
