<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserTableSeeder::class,
            ProductVendorTableSeeder::class,
            ProductTableSeeder::class,
            ProductImageTableSeeder::class,
            ProductImagePivotTableSeeder::class,
            ProductCategoryTableSeeder::class,
            ProductCategoryPivotTableSeeder::class,
        ]);
    }
}
