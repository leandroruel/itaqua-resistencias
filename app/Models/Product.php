<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Searchable;
    
    /**
     * define o nome da tabela
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * array de campos que são preenchiveis
     */
    protected $fillable = ['name', 'description', 'quantity', 'is_active', 'slug', 'specifications', 'vendor_id', 'updated_at', 'created_at'];

    /**
     * define o relacionamento com a model de product_category.
     *
     * @return void
     */
    public function categories()
    {
        return $this->belongsToMany(\App\Models\Category::class, 'product_category')->withPivot('product_id', 'category_id', 'created_at');
    }

    /**
     * define o relacionamento com a model product_image
     *
     * @return void
     */
    public function images()
    {
        return $this->belongsToMany(\App\Models\Image::class, 'product_image')->withPivot('product_id', 'image_id', 'created_at');
    }
}
