<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    /**
     * define o relacionamento com a model product
     *
     * @return void
     */
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class);
    }
}
