<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /**
     * seta o nome da tabela
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * adiciona os campos à propriedade fillable p/ permitir mass assignment
     */
    protected $fillable = ['product_id', 'description', 'created_at', 'updated_at'];

    /**
     * define o relacionamento com a model produtos
     *
     * @return void
     */
    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'product_category')->withPivot('category_id', 'product_id', 'created_at');
    }

    /**
     * define o relacionamento com a model images
     * 
     * @return void
     */
    public function images()
    {
        return $this->belongsToMany(\App\Models\Image::class, 'category_image')->withPivot('category_id', 'image_id', 'created_at');
    }
}
