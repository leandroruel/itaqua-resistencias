<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * seta o nome da tabela
     *
     * @var string
     */
    protected $table = 'image';

    /**
     * define o relacionamento com a model produtos
     *
     * @return void
     */
    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }
}
