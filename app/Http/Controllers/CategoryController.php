<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;

class CategoryController extends Controller
{

    /**
     * lista todos as categorias de produtos 
     */
    public function list()
    {
        $categories = Category::with('images')->get();

        return view('website.produtos', compact('categories'));
    }

    /**
     * mostra a página da categoria
     *
     * @param string $slug
     * @return void
     */
    public function showCategory($slug)
    {
        // obtém a categoria de acordo com a slug que esta na url
        $category = Category::where('slug', $slug)->first();

        // obtém os produtos relacionados à categoria
        $products = Product::with(['images', 'categories' => function($q) use ($slug) {
            $q->where('slug', $slug);
        }])->get();

        // se a categoria nao existir, lança erro 404
        if (!$category) {
            abort('404', 'O item especificado não foi encontrado ou foi movido.');
        }
        
        return view('website.categoria', compact('category', 'products'));
    }
}
