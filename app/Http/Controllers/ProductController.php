<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * mostra a página do produto
     *
     * @param string $slug
     * @return void
     */
    public function showProduct($category_slug, $product_slug)
    {
        $product = Product::where('slug', $product_slug)->with('categories')->first();

        if (!$this->categoryExists($category_slug, $product_slug)) {
            return view('website.errors.404');
        }

        if (!$product) {
            return view('website.errors.404');
        }

        return view('website.produto-item', $product->toArray());
    }

    /**
     * verifica se a categoria acessada via url existe no banco de dados
     *
     * @param string $category_slug
     * @param string $product_slug
     * @return boolean
     */
    public function categoryExists($category_slug, $product_slug)
    {
        $product = Product::where('slug', $product_slug)->with('categories')->first();

        if (!$product) {
            return false;
        }

        return $product->getRelation('categories')->first()->slug === $category_slug;
    }
}
