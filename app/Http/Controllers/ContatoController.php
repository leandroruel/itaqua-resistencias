<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;
use Mail;

class ContatoController extends Controller
{
    public function store(ContactFormRequest $request)
    {
        Mail::send('emails.contato', $request->all(), function ($mail) use ($request) {
            $mail->from($request->email, $request->nome);
            $mail->to(config('mail.from.address'))->subject('Contato Site');
        });

        return redirect('/contato')->with('success', 'Sua mensagem foi enviada, em breve retornaremos o contato.');
    }
}
