<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * caminho para a pasta de imagens
     */
    public $path = '/images/products/';

    /**
     * página inicial que lista todas categorias
     * 
     */
    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));
    }

    public function show()
    {
        //
    }

    /**
     * retorna um formulario para criação de categoria
     * 
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * armazena um registro novo de uma categoria
     * 
     */
    public function store(Request $request)
    {
        $category = new Category();

        $data = [
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'description' => $request->description,
            'created_at' => Carbon::now()
        ];

        $id = $category->insertGetId($data);

        $categoryItem = Category::find($id);

        $categoryItem->images()->attach($request->image_id);

        if (!$id) {
            return redirect('/admin/categories')->with('error', 'Não foi possível cadastrar a categoria');
        }

        return redirect('/admin/categories')->with('success', 'Categoria cadastrada com sucesso');
    }

    /**
     * mostra um formulario para edição da categoria
     * 
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.categories.edit', compact('category'));
    }

    /**
     * atualiza uma determinada categoria
     * 
     */
    public function update(Request $request)
    {
        $categoryItem = Category::find($request->id);

        $data = [
            'description' => $request->description,
            'updated_at' => Carbon::now()
        ];

        if (!$categoryItem->update($data)) {
            return redirect('/admin/categories')->with('error', 'Ocorreu um erro ao atualizar a categoria, tente novamente.');
        }

        return redirect('/admin/categories')->with('success', 'Categoria Atualizada com sucesso!');
    }

    /**
     * deleta uma determinada categoria
     * 
     */
    public function delete(Request $request)
    {
        $category = Category::find($request->id);
        
        if(!$category->delete()) {
            return response()->json([
                'error' => 'Ocorreu um erro ao deletar o item especificado'
            ]);
        }

        return response()->json([
            'success' => 'Item deletado com sucesso!'
        ]);
    }
}
