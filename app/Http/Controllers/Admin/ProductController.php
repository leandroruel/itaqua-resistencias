<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use Carbon\Carbon;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $categories = Category::all();

        return view('admin.products.index', compact('products', 'categories'));
    }

    /**
     * exibe os detalhes do produto
     * @return string
     */
    public function details(Request $request)
    {
        $data = Product::find($request->id);

        if (!$data) {
            return response()->json([
                'error' => 'Nenhum dado encontrado para o item especificado'
            ]);
        }

        return response()->json($data);
    }

    /**
     * renderiza a view do formulario de criação de produto
     * @return void
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.products.create', compact('categories'));
    }

    /**
     * cria um novo produto
     * 
     * @param Illuminate\Http\Request
     * @return string
     */
    public function store(Request $request)
    {
        $product = new Product();
        
        $data = [
            'name' => $request->name,
            'description' => $request->description,
            'specifications' => $request->specifications,
            'vendor_id' => 1,
            'quantity' => $request->quantity,
            'slug' => str_slug($request->name),
            'is_active' => 1,
            'created_at' => Carbon::now()
        ];

        // insere o registro e pega o id criado
        $id = $product->insertGetId($data);

        // verifica se foi inserido com sucesso
        if (!$id) {
            return redirect('/admin/products')->with('error', 'Ocorreu um problema ao adicionar o produto');
        }

        // procura o produto recém criado pelo id
        $productItem = Product::find($id);
        
        // adiciona a categoria ao produto
        $productItem->categories()->attach($request->category);

        // adiciona as imagens ao produto
        foreach ($request->image_id as $key => $value) {
            $productItem->images()->attach($value);
        }

        return redirect('/admin/products')->with('success', 'Produto adicionado com sucesso!');
    }

    /**
     * deleta um produto
     * @return string
     */
    public function delete(Request $request)
    {
        $item = Product::destroy($request->id);

        if (!$item) {
            return response()->json([
                'error' => 'Não foi possível excluir o item especificado'
            ]);
        }

        return response()->json([
            'success' => 'O produto especificado foi excluído com sucesso'
        ]);
    }

    /**
     * mostra um formulario para edição do produto
     * 
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        
        foreach($product->categories as $category) {
            $productCategory = $category;
        }

        return view('admin.products.edit', compact('product', 'categories', 'productCategory'));
    }

    public function update(Request $request)
    {
        $productItem = Product::find($request->product_id);

        $data = [
            'quantity' => $request->quantity,
            'description' => $request->description,
            'specifications' => $request->specifications,
            'updated_at' => Carbon::now()
        ];

        $modelUpdate = $productItem->update($data);

        if (!$modelUpdate) {
            return redirect('/admin/products')->with('error', 'Ocorreu um erro ao atualizar o produto, tente novamente.');
        }

        return redirect('/admin/products')->with('success', 'Produto atualizado com sucesso!');
    }
}
