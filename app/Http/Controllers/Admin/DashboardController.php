<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * retorna a view do dashboard
     *
     * @return void
     */
    public function index()
    {
        return view('admin.home');
    }
}
