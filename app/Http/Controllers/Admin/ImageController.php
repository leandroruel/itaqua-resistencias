<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Image;
use Carbon\Carbon;

class ImageController extends Controller
{
    /**
     * pasta padrão para upload das imagens
     */
    public $path = '/images/products/';

    /**
     * armazena a imagem na pasta especificada e salva o registro na tabela product_image
     * 
     */
    public function store(Request $request)
    {
        $image = new Image();

        // pega o nome do arquivo sem a extensão
        $fileName = explode('.', $request->images->getClientOriginalName())[0];

        // pega a extensão do arquivo
        $fileExtension = $request->images->getClientOriginalExtension();

        $data = [
            'name' => $fileName,
            'extension' => $fileExtension,
            'path' => $this->path,
            'is_default' => 0,
            'alt_text' => 'Imagem meramente ilustrativa',
            'created_at' => Carbon::now()
        ];

        $id = $image->insertGetId($data);

        if (!$id) {
            return response()->json([
                'error' => 'Ocorreu um erro ao salvar a imagem'
            ]);
        }

        $request->images->storeAs($this->path, $request->images->getClientOriginalName());

        return response()->json([
            'data' => [
                'id' => $id,
                'filename' => $fileName,
                'extension' => $fileExtension,
                'message' => 'Imagem salva com sucesso'                
            ]
        ]);
    }

    /**
     * deleta a imagem especificada
     * 
     */
    public function delete(Request $request)
    {
        $image = Image::find($request->id);
        $fullFileName = $image->name . '.' . $image->extension;
        $imagePath = public_path($this->path);

        if (!unlink($imagePath . '/' . $fullFileName)) {
            return response()->json([
                'error' => [
                    'detail' => 'Houve um erro ao deletar o arquivo especificado',
                    'code' => 'FileNotDeleted'
                ]
            ]);
        }

        $image->delete();
        return response()->json([
            'success' => 'Arquivo deletado com sucesso'
        ]);
    }
}
