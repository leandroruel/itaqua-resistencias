<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group([], function () {

    /**
     * Rotas fixas do website
     */
    Route::get('/', function () {
        return view('website.index');
    });

    Route::get('/empresa', function () {
        return view('website.empresa');
    });

    Route::get('/contato', function () {
        return view('website.contato');
    });

    Route::post('/contato/store', 'ContatoController@store');

    Route::get('/eventos', function () {
        return view('website.eventos');
    });

    Route::get('/segmentos', function () {
        return view('website.segmentos');
    });

    Route::get('/categorias', function () {
        return view('website.categorias');
    });

    Route::get('/maquinas', function () {
        return view('website.maquinas');
    });

    Route::get('/blog', function () {
        return view('website.blog');
    });

    Route::get('/informacoes', function () {
        return view('website.informacoes');
    });

    Route::get('/mapa-site', function () {
        return view('website.mapa-site');
    });

    Route::get('/produtos', 'CategoryController@list');

    Route::get('/produtos/{category_slug}', 'CategoryController@showCategory');

    Route::get('/produtos/{category_slug}/{product_slug}', 'ProductController@showProduct');

    Route::get('/search', function (Request $request) {
        return App\Models\Product::search($request->search)->get();
    });

});

/**
 * grupo de rotas para o admin
 */
Route::group([
    'prefix' => 'admin',
    'middleware' => 'auth'
], function ($route) {
    $route->get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');

    // rotas para produtos
    $route->get('/products', 'Admin\ProductController@index')->name('products');
    $route->get('/products/details',  'Admin\ProductController@details');
    $route->get('/products/delete', 'Admin\ProductController@delete');
    $route->get('/products/create', 'Admin\ProductController@create');
    $route->post('/products/store', 'Admin\ProductController@store');
    $route->get('/products/edit/{id}', 'Admin\ProductController@edit');
    $route->post('/products/update', 'Admin\ProductController@update');

    // rotas para categorias
    $route->get('/categories', 'Admin\CategoryController@index')->name('categories');
    $route->get('/categories/create', 'Admin\CategoryController@create')->name('create-category');
    $route->post('/categories/store', 'Admin\CategoryController@store');
    $route->delete('/categories/delete', 'Admin\CategoryController@delete');
    $route->get('/categories/edit/{id}', 'Admin\CategoryController@edit')->name('edit-category');

    // rotas para imagens
    $route->post('/images/store', 'Admin\ImageController@store');
    $route->delete('/images/delete', 'Admin\ImageController@delete');

    // rotas para menu
    $route->get('/menu', 'Admin\MenuController@index');
    $route->get('/menu/create', 'Admin\MenuController@create');
    $route->post('/menu/store', 'Admin\MenuController@store');
    $route->get('/menu/edit/{id}', 'Admin\MenuController@edit');
    $route->post('/menu/update', 'Admin\MenuController@update');

    // rotas para galeria
    $route->get('/gallery', 'Admin\GalleryController@index');

    // rotas para páginas
    $route->get('/pages', 'Admin\PageController@index');

    // rotas para eventos
    $route->get('/events', 'Admin\EventController@index');
    $route->get('/events/create', 'Admin\EventController@create');
    $route->post('/events/store', 'Admin\EventController@store');
    $route->get('/events/edit/{id}', 'Admin\EventController@edit');
    $route->get('/events/update', 'Admin\EventController@update');
    $route->get('/events/delete', 'Admin\EventController@delete');
    $route->get('/events/show/{id}', 'Admin\EventController@show');

    // rotas para blog
    $route->get('/blog', 'Admin\BlogController@index');
    $route->get('/blog/create', 'Admin\BlogController@create');
    $route->post('/blog/store', 'Admin\BlogController@store');
    $route->get('/blog/edit/{id}', 'Admin\BlogController@edit');
    $route->post('/blog/update', 'Admin\BlogController@update');
    $route->delete('/blog/delete', 'Admin\BlogController@delete');
});
