<?php

// Dashboard
Breadcrumbs::for('dashboard', function ($trail) {
    $trail->push('Dashboard', route('dashboard'));
});

// Dashboard > Products
Breadcrumbs::for('products', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Produtos', route('products'));
});

// Dashboard > Products > Create
Breadcrumbs::for('products/create', function ($trail) {
    $trail->parent('products');
    $trail->push('Criar', route('/products/create'));
});

// Dashboard > Categories
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('dashboard');
    $trail->push('Categorias', route('categories'));
});

// Dashboard > Categories > Create
Breadcrumbs::for('create', function ($trail) {
    $trail->parent('categories');
    $trail->push('Criar Categoria', route('create-category'));
});

// Dashboard > Categories > Edit
Breadcrumbs::for('categories/edit', function ($trail) {
    $trail->parent('categories');
    $trail->push('Editar Categoria', route('edit-category', 'id'));
});