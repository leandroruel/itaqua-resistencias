var mobileButton = document.querySelector('.header__mbutton');
var closeButton = document.querySelector('.nav__close');
var mainNavbar = document.getElementById('mainNav');
var menu = document.querySelector('.menu');
var content = document.querySelector('body');

mobileButton.addEventListener('click', function () {
    mainNavbar.classList.toggle('is-expanded');
    menu.setAttribute('aria-hidden', !mainNavbar.classList.contains('is-expanded'));
    content.classList.toggle('menu-is-open');
});

closeButton.addEventListener('click', function () {
    mainNavbar.classList.remove('is-expanded');
    menu.setAttribute('aria-hidden', !mainNavbar.classList.contains('is-expanded'));
    content.classList.remove('menu-is-open');
});